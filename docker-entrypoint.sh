#!/bin/sh
set -e

echo "### Waiting for MySQL to start ###"
until nc -z -v -w30 heroku_db 3306
do
  echo "Waiting for database connection..."
  # wait for 5 seconds before check again
  sleep 5
done

echo "### Starting Up ###"

export JAVA_OPTS="-Xms1024m -Xmx2048m -XX:MaxPermSize=128M -Dfile.encoding=UTF8 -Duser.timezone=GMT+2 -Djava.security.egd=file:/dev/./urandom $JAVA_OPTS"

exec "$@"
