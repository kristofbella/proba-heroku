FROM openjdk:8-jdk-alpine
MAINTAINER all.frontside@frontside.hu

WORKDIR /srv

COPY /build/libs/heroku-*.jar /srv/app.jar
COPY ./docker-entrypoint.sh /usr/local/sbin/
RUN ["chmod", "+x", "/usr/local/sbin/docker-entrypoint.sh"]

VOLUME /tmp
VOLUME /files
EXPOSE 8080

VOLUME /files

ENV JAVA_OPTS=""

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["sh", "-c", "java $JAVA_OPTS -jar /srv/app.jar"]
